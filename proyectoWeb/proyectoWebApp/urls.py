from django.urls import path

from proyectoWebApp import views

urlpatterns = [
    path('',views.home, name="Home"),
    path('ecuaciones', views.EcuacionesNoLineales, name="Ecuaciones"),
    path('biseccion', views.Biseccion, name="Biseccion"),
    path('reglaFalsa', views.ReglaFalsa, name="ReglaFalsa"),
    path('newton', views.Newton, name="Newton"),
    path('secante', views.Secante, name="Secante"),
    path('resultadoBiseccion', views.ResultadoBiseccion, name='ResultadoBiseccion'),
    path('graficaBiseccion', views.GraficaBiseccion, name='GraficaBiseccion'),
    path('resultadoReglaFalsa', views.ResultadoReglaFalsa, name='ResultadoReglaFalsa'),
    path('graficaReglaFalsa', views.GraficaReglaFalsa, name='GraficaReglaFalsa'),
    path('resultadoNewton', views.ResultadoNewton, name='ResultadoNewton'),
    path('graficaNewton', views.GraficaNewton, name='GraficaNewton'),
    path('resultadoSecante', views.ResultadoSecante, name='ResultadoSecante'),
    path('graficaSecante', views.GraficaSecante, name='GraficaSecante'),

    path('derivadas', views.Derivadas, name='Derivadas'),
    path('dosPuntos', views.DosPuntos, name='DosPuntos'),
    path('resultadoFormularDosPuntos', views.ResultadoDosPuntos, name='ResultadoDosPuntos'),
    path('tresPuntos', views.TresPuntos, name='TresPuntos'),
    path('resultadoFormularTresPuntos', views.ResultadoTresPuntos, name='ResultadoTresPuntos'),
    path('cincoPuntos', views.CincoPuntos, name='CincoPuntos'),
    path('resultadoFormularCincoPuntos', views.ResultadoCincoPuntos, name='ResultadoCincoPuntos'),

    path('integrales', views.Integrales, name='Integrales'),
    path('reglaTrapecio', views.ReglaTrapecio, name='ReglaTrapecio'),
    path('resultadoReglaTrapecio', views.ResultadoReglaTrapecio, name='ResultadoReglaTrapecio'),
    path('reglaSimpson', views.ReglaSimpson, name='ReglaSimpson'),
    path('resultadoReglaSimpson', views.ResultadoReglaSimpson, name='ResultadoReglaSimpson'),

]