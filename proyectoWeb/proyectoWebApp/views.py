from django.shortcuts import render, HttpResponse
from proyectoWeb.ecuaciones.biseccion import calcularBiseccion
from proyectoWeb.ecuaciones.reglaFalsa import calcularReglaFalsa
from proyectoWeb.ecuaciones.newton import calcularNewton
from proyectoWeb.ecuaciones.secante import calcularSecante
from proyectoWeb.derivadas.dosPuntos import calcularDosPuntos
from proyectoWeb.derivadas.tresPuntos import calcularTresPuntos 
from proyectoWeb.derivadas.cincoPuntos import calcularCincoPuntos
from proyectoWeb.integrales.reglaTrapecio import calcularReglaTrapecio
from proyectoWeb.integrales.reglaSimpson import calcularReglaSimpson
import matplotlib.pyplot as plt
import io
import numpy as np
import re




# Create your views here.
#global div_tabla

def home(request):
    return render(request, "proyectoWebApp/home.html")

def EcuacionesNoLineales(request):
    return render(request, "proyectoWebApp/ecuaciones.html")
##--------------------------------
result = ""
def Biseccion(request):

    return render(request, "proyectoWebApp/biseccion.html")


def ResultadoBiseccion(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularBiseccion(form_data)
        if(float(form_data['a'])> float(form_data['b'])):
         tabla_html = ""
        else:
         tabla_html = result.to_html()
        
        return render(request, 'proyectoWebApp/biseccion.html', {'tabla': tabla_html})
    else:
        return render(request, 'proyectoWebApp/biseccion.html')

def GraficaBiseccion(request):
    fig, ax = plt.subplots()
    iter=[]
    for i in range(len(result['Error'])):
     iter.append(i)
    ax.plot(iter, result['Error'])
    plt.title("Grafica error")
    plt.xlabel("Iteracciones")
    plt.ylabel("Porcentaje error")
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    response = HttpResponse(buf.read(), content_type='image/png')
    response['Content-Disposition'] = 'attachment; filename="mi_grafica.png"'
    return response
    
##--------------------------------
def ReglaFalsa(request):
    return render(request, "proyectoWebApp/reglaFalsa.html")

resultRegla = ""
def ResultadoReglaFalsa(request):
    if request.method == 'POST':
        form_data = request.POST
        global resultRegla
        resultRegla = calcularReglaFalsa(form_data)
        if(float(form_data['a'])> float(form_data['b'])):
         tabla_html = ""
        else:
         tabla_html = resultRegla.to_html()
        
        return render(request, 'proyectoWebApp/reglaFalsa.html', {'tablaRegla': tabla_html})
    else:
        return render(request, 'proyectoWebApp/reglaFalsa.html')

def GraficaReglaFalsa(request):
    fig, ax = plt.subplots()
    iter=[]
    for i in range(len(resultRegla['Error'])):
     iter.append(i)
    ax.plot(iter, resultRegla['Error'])
    plt.title("Grafica error")
    plt.xlabel("Iteracciones")
    plt.ylabel("Porcentaje error")
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    response = HttpResponse(buf.read(), content_type='image/png')
    response['Content-Disposition'] = 'attachment; filename="mi_grafica.png"'
    return response
    
##--------------------------------

def Newton(request):
    return render(request, "proyectoWebApp/newton.html")

resultNewton = ""
def ResultadoNewton(request):
    if request.method == 'POST':
        print(request.POST.get('funcion'))
        if(re.match("^-?\d+\.?\d*$", request.POST.get('funcion'))):
           tabla_html = ""
        else:
           form_data = request.POST
           global resultNewton
           resultNewton = calcularNewton(form_data)
           tabla_html = resultNewton.to_html()
        
        
        return render(request, 'proyectoWebApp/newton.html', {'tablaNewton': tabla_html})
    else:
        return render(request, 'proyectoWebApp/newton.html')

def GraficaNewton(request):
    fig, ax = plt.subplots()
    iter=[]
    for i in range(len(resultNewton['error'])):
     iter.append(i)
    ax.plot(iter, resultNewton['error'])
    plt.title("Grafica error")
    plt.xlabel("Iteracciones")
    plt.ylabel("Porcentaje error")
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    response = HttpResponse(buf.read(), content_type='image/png')
    response['Content-Disposition'] = 'attachment; filename="mi_grafica.png"'
    return response
    
##--------------------------------

def Secante(request):
    return render(request, "proyectoWebApp/secante.html")

resultSecante = ""
def ResultadoSecante(request):
    if request.method == 'POST':
        form_data = request.POST
        global resultSecante
        resultSecante = calcularSecante(form_data)
        tabla_html = resultSecante.to_html()
        
        return render(request, 'proyectoWebApp/secante.html', {'tablaSecante': tabla_html})
    else:
        return render(request, 'proyectoWebApp/secante.html')

def GraficaSecante(request):
    fig, ax = plt.subplots()
    iter=[]
    for i in range(len(resultSecante['Error'])):
     iter.append(i)
    ax.plot(iter, resultSecante['Error'])
    plt.title("Grafica Secante")
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    response = HttpResponse(buf.read(), content_type='image/png')
    response['Content-Disposition'] = 'attachment; filename="mi_grafica.png"'
    return response


##---------------------------------------------------------------------------
def Derivadas(request):
    return render(request, "proyectoWebApp/derivadas.html")

def DosPuntos(request):
    return render(request, "proyectoWebApp/formulaDosPuntos.html")

def ResultadoDosPuntos(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularDosPuntos(form_data)
        tabla = result.to_html
        
        return render(request, 'proyectoWebApp/formulaDosPuntos.html', {'tabla': tabla})
    else:
        return render(request, 'proyectoWebApp/formulaDosPuntos.html')

##---------------------------------------------------------------------------    
def TresPuntos(request):
    return render(request, "proyectoWebApp/formulaTresPuntos.html")

def ResultadoTresPuntos(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularTresPuntos(form_data)
        tabla = result.to_html
        
        return render(request, 'proyectoWebApp/formulaTresPuntos.html', {'tabla': tabla})
    else:
        return render(request, 'proyectoWebApp/formulaTresPuntos.html')
    
    ##---------------------------------------------------------------------------  
      
def CincoPuntos(request):
    return render(request, "proyectoWebApp/formulaCincoPuntos.html")

def ResultadoCincoPuntos(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularCincoPuntos(form_data)
        tabla = result.to_html
        
        return render(request, 'proyectoWebApp/formulaCincoPuntos.html', {'tabla': tabla})
    else:
        return render(request, 'proyectoWebApp/formulaCincoPuntos.html')
    


##---------------------------------------------------------------------------
def Integrales(request):
    return render(request, "proyectoWebApp/integrales.html")

def ReglaTrapecio(request):
    return render(request, "proyectoWebApp/reglaTrapecio.html")

def ResultadoReglaTrapecio(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularReglaTrapecio(form_data)
        if(float(form_data['inferior'])> float(form_data['superior'])):
         tabla = ""
        else:
         tabla = result.to_html()
        
        return render(request, 'proyectoWebApp/reglaTrapecio.html', {'tabla': tabla})
    else:
        return render(request, 'proyectoWebApp/reglaTrapecio.html')
    
##---------------------------------------------------------------------------

def ReglaSimpson(request):
    return render(request, "proyectoWebApp/reglaSimpson.html")

def ResultadoReglaSimpson(request):
    if request.method == 'POST':
        form_data = request.POST
        global result
        result = calcularReglaSimpson(form_data)
        if(float(form_data['inferior'])> float(form_data['superior'])):
         tabla_html = ""
        else:
         tabla_html = result.to_html()
        
        return render(request, 'proyectoWebApp/reglaSimpson.html', {'tabla': tabla_html})
    else:
        return render(request, 'proyectoWebApp/reglaSimpson.html')

"""Las siguientes lineas estan comentadas
hasta definir las siguientes opciones de la app
def home(request):
    return HttpResponse("Incio")

def home(request):
    return HttpResponse("Incio")"""            
