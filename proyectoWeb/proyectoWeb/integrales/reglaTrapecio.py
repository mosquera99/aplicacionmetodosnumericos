import math
import pandas as pd
import sympy as sp

def calcularReglaTrapecio(form_data):

 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 inferior = float(form_data['inferior'])
 superior = float(form_data['superior'])
 numero = float(form_data['puntos'])

 h=(superior-inferior)/numero
 
 tabla=pd.DataFrame()
 tabla['x']=None
 tabla['f(x)']=None
 tabla['f(x*2)']=None
 tabla['Integral']=None

 vector_X=[]
 vector_FX=[]
 vector_FX2=[]
 vector_ITG=[]
 cont=0
 valorX=0
 valorX2 = 0
 sumatoria=0

 while valorX <=superior:
  
  if cont==0:
   valorX =inferior
   vector_X.append(valorX)
   fx=funcion.subs(x,valorX).evalf()
   vector_FX.append(fx)
   vector_FX2.append(fx)
   sumatoria = sumatoria+fx
   vector_ITG.append("SIGUE")
  if cont == numero-1:
   valorX=valorX+h
   vector_X.append(valorX)
   fx=funcion.subs(x,valorX).evalf()
   vector_FX.append(fx)
   vector_FX2.append(fx)
   sumatoria = sumatoria+fx
  else:
   valorX=valorX+h
   vector_X.append(valorX)
   fx=funcion.subs(x,valorX).evalf()
   valorX2=fx*2
   vector_FX.append(fx)
   vector_FX2.append(valorX2)
   sumatoria = sumatoria+valorX2
   vector_ITG.append("SIGUE")
   
  cont=cont+1
 integral = h/2*sumatoria
 vector_ITG.append(integral)
 print("integral" +str(integral))
 tabla['x']=pd.Series(vector_X)
 tabla['f(x)']=pd.Series(vector_FX)
 tabla['f(x*2)']=pd.Series(vector_FX2)
 tabla['Integral']=pd.Series(vector_ITG)

 return tabla