import math
import pandas as pd
import sympy as sp

def calcularDosPuntos(form_data):
   #Función (exp(1)**(-(x**2)+2))-(x*(sin(2*x-3)))+1
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 valorX = float(form_data['valor'])
 tolerancia = float(form_data['tolerancia'])

 tabla=pd.DataFrame()
 tabla['h']=None
 tabla['f(x+h)']=None
 tabla['f(x)']=None
 tabla['Derivada']=None
 vector_H=[]
 vector_XH=[]
 vector_FX=[]
 vector_DERIVADA=[]

 inicial = 1.0
 cont = 0

 while tolerancia <= inicial:
  
  #cont = cont+1
  vector_H.append(inicial)

  suma = valorX+inicial

  fa=funcion.subs('x',suma).evalf()
  vector_XH.append(float(str(fa).rstrip('0').rstrip('.')))

  fx=funcion.subs('x',valorX).evalf()
  vector_FX.append(float(str(fx).rstrip('0').rstrip('.')))
  
  derivada = (fa-fx)/inicial
  vector_DERIVADA.append(float(str(derivada).rstrip('0').rstrip('.')))

  inicial = inicial/10

 tabla['h']=vector_H
 tabla['f(x)']=vector_FX
 tabla['f(x+h)']=vector_XH
 tabla['Derivada']=vector_DERIVADA

 return tabla