import math
import pandas as pd
import sympy as sp

def calcularTresPuntos(form_data):
   #Función (exp(1)**(-(x**2)+2))-(x*(sin(2*x-3)))+1
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 valorX = float(form_data['valor'])
 tolerancia = float(form_data['tolerancia'])

 tabla=pd.DataFrame()
 tabla['h']=None
 tabla['f(x+2h)']=None
 tabla['f(x+h)']=None
 tabla['f(x)']=None
 tabla['Derivada']=None
 vector_H=[]
 vector_X2H=[]
 vector_XH=[]
 vector_FX=[]
 vector_DERIVADA=[]

 inicial = 0.5
 cont = 0

 while tolerancia <= inicial:
  
  #cont = cont+1
  vector_H.append(inicial)

  suma = valorX+inicial
  suma2 = valorX+2*inicial
  f2x=funcion.subs('x',suma2).evalf()
  vector_X2H.append(float(str(f2x).rstrip('0').rstrip('.')))

  fa=funcion.subs('x',suma).evalf()
  vector_XH.append(float(str(fa).rstrip('0').rstrip('.')))

  fx=funcion.subs('x',valorX).evalf()
  vector_FX.append(float(str(fx).rstrip('0').rstrip('.')))
  
  derivada = 1/(2*inicial)*(-3*fx+4*fa-f2x)
  print(derivada)
  vector_DERIVADA.append(float(str(derivada).rstrip('0').rstrip('.')))

  inicial = inicial/10

 tabla['h']=vector_H
 tabla['f(x)']=vector_FX
 tabla['f(x+2h)']=vector_X2H
 tabla['f(x+h)']=vector_XH
 tabla['Derivada']=vector_DERIVADA

 return tabla