import math
import pandas as pd
import sympy as sp

def calcularCincoPuntos(form_data):
   
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 valorX = float(form_data['valor'])
 tolerancia = float(form_data['tolerancia'])

 tabla=pd.DataFrame()
 tabla['h']=None
 tabla['f(x-2h)']=None
 tabla['f(x-h)']=None
 tabla['f(x+h)']=None
 tabla['f(x+2h)']=None
 tabla['Derivada']=None
 vector_H=[]
 vector_X1=[]
 vector_X2=[]
 vector_X3=[]
 vector_X4=[]
 vector_DERIVADA=[]

 inicial = 0.1
 cont = 0

 while tolerancia <= inicial:
  
  #cont = cont+1
  vector_H.append(inicial)

  suma = valorX+2*inicial
  suma2 = valorX-2*inicial
  resta1 = valorX+inicial
  resta2 = valorX-inicial

  f2=funcion.subs('x',suma).evalf()
  vector_X1.append(float(str(f2).rstrip('0').rstrip('.')))

  f1=funcion.subs('x',suma2).evalf()
  vector_X2.append(float(str(f1).rstrip('0').rstrip('.')))

  f3=funcion.subs('x',resta1).evalf()
  vector_X3.append(float(str(f3).rstrip('0').rstrip('.')))

  f4=funcion.subs('x',resta2).evalf()
  vector_X4.append(float(str(f4).rstrip('0').rstrip('.')))
  
  derivada = (f1-8*f4+8*f3-f2)/(12*inicial)
 
  vector_DERIVADA.append(float(str(derivada).rstrip('0').rstrip('.')))

  inicial = inicial/10


 tabla['h']=vector_H
 tabla['f(x+2h)']=vector_X1
 tabla['f(x-2h)']=vector_X2
 tabla['f(x+h)']=vector_X3
 tabla['f(x-h)']=vector_X4
 tabla['Derivada']=vector_DERIVADA

 return tabla