import pandas as pd
import sympy as sp

def calcularSecante(form_data):
 #ingreso de la función matemática
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])#exp(1)**(-x)-x

 x0= float(form_data['uno']) 
 x1= float(form_data['dos'])

 fx0=funcion.subs('x',x0).evalf()
 fx1=funcion.subs('x',x1).evalf()


 #Definición de tolerancia de error
 tolerancia = float(form_data['tolerancia'])
 error=float(1)


 #metodo regla falsa:
 tabla=pd.DataFrame()
 tabla['x']=None
 tabla['fx']=None
 tabla['Error']=None
 lista_x=[]
 lista_fx=[]
 lista_Error=[]
 4

 print(tolerancia, error)

 lista_x.append(x0)
 lista_fx.append(fx0)
 lista_Error.append(error)

 lista_x.append(x1)
 lista_fx.append(fx1)
 lista_Error.append(error)

 while tolerancia < error:
    x2=x1-fx1*(x1-x0)/(fx1-fx0)
    error= abs(x2-x1)
    fx2=funcion.subs('x',x2).evalf()
    x0=x1
    x1=x2
    fx0=funcion.subs('x',x0).evalf()
    fx1=funcion.subs('x',x1).evalf()
    lista_x.append(x2)
    lista_fx.append(fx2)
    lista_Error.append(error)


 tabla['x']=lista_x
 tabla['fx']=lista_fx
 tabla['Error']=lista_Error
 tabla
 return tabla