import math
import pandas as pd
import sympy as sp

def calcularNewton(form_data):
 #exp(1)**(-x)-x
 x=sp.symbols('x')

 funcion = sp.sympify(form_data['funcion'])
 derivada = sp.diff(funcion,x)
 inicial = float(form_data['valorInicial'])
 tolerancia = float(form_data['tolerancia'])
 error=float(1)

 #Definición tabla
 tabla=pd.DataFrame()
 tabla['x']=None
 tabla['fx']=None
 tabla['fxp']=None
 tabla['error']=None
 vector_X=[]
 vector_FX=[]
 vector_FXP=[]
 vector_ERROR=[]


 def validarSiga(valor,tolerancia):
  mensaje = "";
  if(valor<tolerancia):
    mensaje = 'PARE'
  else:
    mensaje = 'SIGA'

  return mensaje

 def calcularError(limit_a,limit_b):
    return abs(limit_a-limit_b)

 bandera = 'SIGA'
 cont = 1
 while bandera == 'SIGA':
   
   if(cont==1):
     x=inicial
   else:
     inicial = x
     x = x-fx/fxp
     error=calcularError(x,inicial)
   fx=funcion.subs('x',x).evalf()
   fxp=derivada.subs('x',x).evalf()
   vector_X.append(x)
   vector_FX.append(fx)  
   vector_FXP.append(fxp)
   vector_ERROR.append(error)
   if validarSiga(error,tolerancia)  == 'PARE':
    bandera= 'PARE'
   cont=cont+1

 tabla['x']=vector_X
 tabla['fx']=pd.Series(vector_FX)
 tabla['fxp']=pd.Series(vector_FXP)
 tabla['error']=pd.Series(vector_ERROR)
 tabla
 return tabla