from copy import Error
import math
import pandas as pd
import sympy as sp


#Función (exp(1)**(-(x**2)+2))-(x*(sin(2*x-3)))+1
def calcularReglaFalsa(form_data):
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 a = float(form_data['a'])
 b = float(form_data['b'])
 tolerancia = float(form_data['tolerancia'])
 error=float(1)


 #Definición tabla
 tabla=pd.DataFrame()
 tabla['a']=None
 tabla['b']=None
 tabla['xm']=None
 tabla['fa']=None
 tabla['fb']=None
 tabla['fxm']=None
 tabla['Error']=None
 vector_A=[]
 vector_B=[]
 vector_XM=[]
 vector_fa=[]
 vector_fxm=[]
 vector_fb=[]
 vector_Error=[]
 vector_A.append(a)
 vector_B.append(b)

 def validarSiga(valor,tolerancia):
  mensaje = "";
  if(valor<tolerancia):
    mensaje = 'PARE'
  else:
    mensaje = 'SIGA'

  return mensaje

 def calcularError(limit_a,limit_b):
    return abs(limit_a-limit_b)

 bandera = 'SIGA'
 while bandera == 'SIGA':
  #función evaluada en a
  fa=funcion.subs('x',a).evalf()
  vector_fa.append(fa)
  #funcion evaluada en b
  fb=funcion.subs('x',b).evalf()
  vector_fb.append(fb)
  #funcion evaluada en xm
  anteriorXm= a-fa*(b-a)/(fb-fa)
  vector_XM.append(anteriorXm)
  #funcion evaluada en fxm
  fxm=funcion.subs('x',anteriorXm).evalf()
  vector_fxm.append(fxm)
 
  if (fa*fxm)<0:
    a=a
    b=anteriorXm
    xm=a-fa*(b-a)/(fb-fa)
    vector_A.append(a)
    vector_B.append(b)
   
  else:
    a=anteriorXm
    b=b
    xm=a-fa*(b-a)/(fb-fa)
    vector_A.append(a)
    vector_B.append(b)
  error=calcularError(anteriorXm,xm)
  vector_Error.append(error)
  #print("validacion "+str(calcularError(anteriorXm,xm),tolerancia))
  if validarSiga(calcularError(anteriorXm,xm),tolerancia)  == 'PARE':
    bandera= 'PARE'

 vector_XM.append(anteriorXm)
 fa=funcion.subs('x',a).evalf()
 vector_fa.append(fa)
 fb=funcion.subs('x',b).evalf()
 vector_fb.append(fb)
 fxm=funcion.subs('x',anteriorXm).evalf()
 vector_fxm.append(fxm)
 error=calcularError(anteriorXm,xm)
 vector_Error.append(error)
  
 #tabla = tabla.fillna(0)
 tabla['a']=vector_A
 tabla['b']=vector_B
 tabla['xm']=pd.Series(vector_XM)
 tabla['fa']=pd.Series(vector_fa)
 tabla['fb']=pd.Series(vector_fb)
 tabla['fxm']=pd.Series(vector_fxm)
 tabla['Error']=pd.Series(vector_Error)

 tabla
 return tabla