from copy import Error
import math
import pandas as pd
import sympy as sp

def calcularBiseccion(form_data):
   #Función (exp(1)**(-(x**2)+2))-(x*(sin(2*x-3)))+1
 x=sp.symbols('x')
 funcion = sp.sympify(form_data['funcion'])
 a = float(form_data['a'])
 b = float(form_data['b'])
 tolerancia = float(form_data['tolerancia'])
 #print("resultado" +str(a))

#Ingreso de variables por el usuario
 error=float(1)

#calculo xm inicial
 xm= (a+b)/2

#Definición tabla
 tabla=pd.DataFrame()
 tabla['a']=None
 tabla['xm']=None
 tabla['b']=None
 tabla['fa']=None
 tabla['fxm']=None
 tabla['fb']=None
 tabla['Error']=None
 vector_A=[]
 vector_B=[]
 vector_XM=[]
 vector_fa=[]
 vector_fxm=[]
 vector_fb=[]
 vector_Error=[]
 vector_A.append(a)
 vector_B.append(b)
 vector_XM.append(xm)
 vector_Error.append(error)

#Proceso
 while tolerancia < error:
   xmAnterior=xm #actualizar xm en el ciclo  
   fa=funcion.subs('x',a).evalf()
   vector_fa.append(fa)
  #función evaluada en a
   fb=funcion.subs('x',b).evalf()
   vector_fb.append(fb)
  #funcion evaluada en b
   fxm=funcion.subs('x',xm).evalf()
   vector_fxm.append(fxm)
  #funcion evaluada en xm

   if (fa*fxm)<0:
     a=a
     b=xm
     xm=(a+b)/2
     error= abs(xmAnterior-xm)
     vector_A.append(a)
     vector_B.append(b)
     vector_XM.append(xm)
     vector_Error.append(error)
   else:
     a=xm
     b=b
     xm=(a+b)/2
     error= abs(xmAnterior-xm)
     vector_A.append(a)
     vector_B.append(b)
     vector_XM.append(xm)
     vector_Error.append(error)

 fa=funcion.subs('x',a).evalf()
 vector_fa.append(fa)
 
 fb=funcion.subs('x',b).evalf()
 vector_fb.append(fb)
 
 fxm=funcion.subs('x',xm).evalf()
 vector_fxm.append(fxm)
#tabla = tabla.fillna(0)
 tabla['a']=vector_A
 tabla['b']=vector_B
 tabla['xm']=vector_XM
 tabla['Error']=vector_Error
 tabla['fa']=pd.Series(vector_fa)
 tabla['fb']=pd.Series(vector_fb)
 tabla['fxm']=pd.Series(vector_fxm)
 tabla
 #tabla_html = tabla.to_html()
 print(tabla)
 return tabla
  
